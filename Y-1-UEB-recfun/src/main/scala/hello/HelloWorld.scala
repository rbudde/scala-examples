package hello

import scala.collection.JavaConversions._

object HelloWorld extends App {
  val names = System.getProperties().propertyNames()
  (names)map (n ⇒ "" + n) filter (n ⇒ n.startsWith("java")) map (n ⇒ ((n, System.getProperty(n)))) foreach println
  //        Any->String   filter prefix                     map to a pair                         apply   that
}