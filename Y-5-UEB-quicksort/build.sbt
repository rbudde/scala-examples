name := "Y-5-UEB-quicksort"
 
version := "1.0"

retrieveManaged := true

scalaVersion := "2.10.1-RC2"
 
autoCompilerPlugins := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "junit" % "junit" % "4.10" % "test"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0.M5b" % "test"

libraryDependencies += "org.hamcrest" % "hamcrest-all" % "1.1" % "test" 

libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.1-RC2"

// libraryDependencies += compilerPlugin("org.scala-lang.plugins" % "continuations" % "2.10.0")

// libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.1.1"

// scalacOptions += "-P:continuations:enable"
