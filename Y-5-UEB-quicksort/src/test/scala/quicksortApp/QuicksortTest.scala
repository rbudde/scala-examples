package quicksortApp

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class QuicksortSuite extends FunSuite {
  test("empty") {
    assert(Main.quicksort(List()) === List())
  }

  // weitere Tests
}
