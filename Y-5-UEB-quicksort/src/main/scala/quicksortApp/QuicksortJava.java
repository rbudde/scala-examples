package quicksortApp;

import java.util.Arrays;

public class QuicksortJava {
	private int[] xs;

	public void sort(int[] values) {
		if (values == null || values.length == 0) {
			return;
		}
		this.xs = values;
		quicksort(0, values.length - 1);
	}

	private void swap(int i, int j) {
		int temp = xs[i];
		xs[i] = xs[j];
		xs[j] = temp;
	}

	private void quicksort(int low, int high) {
		int i = low, j = high;
		// Get the pivot element from the middle of the list
		int pivot = xs[low + (high - low) / 2];
		// Divide into two lists
		while (i <= j) {
			// If the current value from the left list is smaller then the pivot
			// element then get the next element from the left list
			while (xs[i] < pivot)
				i++;
			// If the current value from the right list is larger then the pivot
			// element then get the next element from the right list
			while (xs[j] > pivot)
				j--;
			// If we have found a values in the left list which is larger then
			// the pivot element and if we have found a value in the right list
			// which is smaller then the pivot element then we exchange the
			// values. As we are done we can increase i and j
			if (i <= j) {
				swap(i, j);
				i++;
				j--;
			}
		}
		// Recursion
		if (low < j)
			quicksort(low, j);
		if (i < high)
			quicksort(i, high);
	}

	/**
	 * nicht die beste Methode zum Ausprobieren, ...
	 */
	public static void main(String[] args) {
		int[] ia = { 1, 3, 5, 3, 3, 2, 4, 6 };
		QuicksortJava sort = new QuicksortJava();
		sort.sort(ia);
		System.out.println("Ergebnis: " + Arrays.toString(ia));
	}
}